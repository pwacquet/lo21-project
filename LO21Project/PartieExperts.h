#pragma once
#include "PartieTactique.h"

namespace SchottenTotten {
	class PartieExperts : public PartieTactique {
	private:
		bool prochainTourEvaluer{ false };
	public:
		void lancer() override;
	};
}