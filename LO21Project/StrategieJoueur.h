#pragma once
#include "outils.h"
//Design Pattern Strategy
namespace SchottenTotten {
    class StrategieJoueur {
    public:
        virtual ~StrategieJoueur() {}
        virtual int choisirIndiceCarte(int nbCartesEnMain) = 0;
        virtual int choisirIndiceBorne() = 0;
        virtual int choisirPuissance() = 0; // pour troupes d'elites
        virtual int choisirCouleur() = 0; // pour Joker par ex
        virtual int choisirSiTirerCarteTactique() = 0;
        virtual int choisir(int debut, int fin, const std::string& message) = 0;
        virtual int choisirConstrained(const std::vector<int>& options) = 0;
    };

    class StrategieRandom : public StrategieJoueur {
    public:
        int choisirIndiceCarte(int nbCartesEnMain) override {
            int nbCartesEnMainInt = static_cast<int>(nbCartesEnMain);
            int indiceCarte{ randomInput<int>(0, nbCartesEnMainInt -1) };
            return indiceCarte;
        }
        int choisirIndiceBorne() override {
            int indiceBorne{ randomInput<int>(0, 8) };
            return indiceBorne;
        }
        int choisirPuissance() override {
            return randomInput<int>(1, 9);
        }
        int choisirCouleur() override {
            return randomInput<int>(0, 5);
        }
        int choisirSiTirerCarteTactique() override {
            return randomInput<int>(0, 1);
        }
        int choisir(int debut, int fin, const std::string& message) override {
            return randomInput<int>(debut, fin) ;
        }
        int choisirConstrained(const std::vector<int>& options) {
            return randomInputConstrained<int>(options);
        }
    };

    class StrategieManuelle : public StrategieJoueur {
    public:
        int choisirIndiceCarte(int nbCartesEnMain) override {
            std::cout << std::endl;
            int nbCartesEnMainInt = static_cast<int>(nbCartesEnMain);
            int indiceCarte{ askUserInput<int>(0, nbCartesEnMainInt -1, "Veuillez choisir une carte: ") };
            return indiceCarte;
        }
        int choisirIndiceBorne() override {
            std::cout << "\n\n";
            int indiceBorne{ askUserInput<int>(0, 8, "Veuillez choisir une borne: ") };
            return indiceBorne;
        }
        int choisirPuissance() override {
            std::cout << std::endl;
            return askUserInput<int>(1, 9, "Choisissez la puissance: ");
        }
        int choisirCouleur() override {
            std::cout << std::endl;
            return askUserInput<int>(0, 5, "Choisissez la couleur: 0. VERT     1. BLEU\n2. ROUGE   3. JAUNE    4. VIOLET   5. BRUN\n");
        }
        int choisirSiTirerCarteTactique() override {
            std::cout << std::endl;
            std::cout << "Est-ce que vous voulez tirer une carte de la pioche Tactique?\n";
            return askUserInput<int>(0, 1, "Tapez 1 si oui, 0 sinon: ");
        }
        int choisir(int debut, int fin, const std::string& message) override {
            std::cout << std::endl;
            return askUserInput<int>(debut, fin, message);
        }
        int choisirConstrained(const std::vector<int>& options) {
            return askUserInputConstrained<int>(options);
        }
    };

}

