#pragma once
#include "outils.h"
#include "Joueur.h"
#include "Borne.h"
#include "Pioche.h"
#include "StrategieJoueur.h"
#include "Defausse.h"

namespace SchottenTotten {
	class Partie {
	protected: //debug
		Joueur joueur1 = { 1 }; // le premier joueur qui joue
		Joueur joueur2 = { 2 };
		int nombreDeCartesChaqueJoueur{ 6 }; 
		PiocheClan piocheClan;
		std::vector<Borne> bornes = std::vector<Borne>(9, Borne());
		//// Singleton:
		//static Partie* instanceUnique;
		//Partie() {};
		//Partie(const Partie&) = delete;
		//virtual ~Partie();
		//void operator=(const Partie&) = delete;
	public:
		//static Partie& donneInstance();
		//static void libereInstance();

		void dealCartesClan();
		void getIndicesBornesJoueesUnCote(int indiceJoueur, std::vector<int>& bornesJoueesUnCote);
		void getIndicesBornesPasRevendiquees(std::vector<int>& bornesNonRevendiquees);
		void getIndicesBornesPasRemplieUnCote(int indiceJoueur, std::vector<int>& getBornesPasRemplieUnCote);
		int quiGagneCinqBornesDispersees(); // retourne numJoueurGagnant 
		int quiGagneTroisBornesAdjacentes(); // retourne numJoueurGagnant

		virtual void gererCarte(int& indiceCurrentBorne, int& indiceCarte, int indiceJoueur); //voir si carte jouable sur une borne et la deposer
		//int pass par reference parce qu'on permet joueur a rejouer
		void setStrategieJoueur(int indiceJoueur, StrategieJoueur* strat) {
			if (indiceJoueur == 1) joueur1.setStrategieJoueur(strat);
			else if (indiceJoueur == 2) joueur2.setStrategieJoueur(strat);
		}
		void quandBorneRemplie(int indiceCurrentBorne); //quand une borne est remplie et pret a etre reclamee
		virtual void printInfoPioche();
		void printWinner();
		virtual void lancer();
		virtual void printAllBornes();
	};
}