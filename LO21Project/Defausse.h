#pragma once
#include "Carte.h"
//factory?

namespace SchottenTotten {
	class Defausse {
		std::vector<const Carte*> defausse;
	public:
		void ajouterCarte(const Carte* carte) {
			if (carte == nullptr) {
				std::cout << "\nErreur fatale defausse\n";
				return;
			}
			defausse.push_back(carte);
		}
		void printCartes() {
			std::cout << "Defausse: \n";
			for (int i = 0; i < defausse.size(); i++) {
				std::cout << i << ".  ";
				defausse[i]->print();
			}
		}
	};

}