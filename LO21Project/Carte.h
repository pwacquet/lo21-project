#pragma once
#include "outils.h"

namespace SchottenTotten {
    class Frontiere;
    class Joueur;
    class Partie;

    enum TypeCarte { CLAN, TACTIQUE };
    class Carte {
    public:
        virtual void print() const {};
        virtual TypeCarte quelTypeCarte() const = 0;
    };
    
    enum Puissance {
        ZERO, UN, DEUX, TROIS, QUATRE, CINQ, SIX, SEPT, HUIT, NEUF
    };
    std::ostream& operator<< (std::ostream& strm, Puissance puiss);
    enum Couleur {
        VERT, BLEU, ROUGE, JAUNE, VIOLET, BRUN
    };
    std::ostream& operator<< (std::ostream& strm, Couleur couleur);
    
    class CarteClan : public Carte {
        Puissance puissance;
        Couleur couleur;
    public:
        void print() const;
        TypeCarte quelTypeCarte() const;
        CarteClan() = default;
        CarteClan(Puissance p, Couleur c) : puissance(p), couleur(c) {}
        Puissance getPuissance() const { return puissance; }
        Couleur getCouleur() const { return couleur; }
        std::string testPrint() {
            std::string nouveau = puissance + " " + couleur;
            return nouveau;
        }
        friend std::ostream& operator<< (std::ostream& strm, const CarteClan& carte);
    };

    enum Capacite {
        JOKER1, JOKER2, ESPION, PORTEBOUCLIER, COLINMAILLLARD, COMBATDEBOUE, CHASSEUR, STRATEGE, BANSHEE, TRAITRE
    };
    std::ostream& operator<< (std::ostream& strm, Capacite tac);
    
    class CarteTactique : public Carte {
        Capacite typeTactique;
    public:
        void print() const;
        TypeCarte quelTypeCarte() const;
        Capacite getTypeCarteTactique() const { return typeTactique; }
        CarteTactique() = default;
        CarteTactique(Capacite n) : typeTactique(n) {}
        friend std::ostream& operator<< (std::ostream& strm, const CarteTactique& carte);
    };
}


