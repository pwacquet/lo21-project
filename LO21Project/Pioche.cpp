#include "Pioche.h"
#include <algorithm>
#include <random>

namespace SchottenTotten {
    PiocheClan::PiocheClan() {
        const int taillePioche = 54; // taille de la pioche du jeu classique
        initialPioche = new const CarteClan * [taillePioche];
        int i = 0;
        for (int puissance = UN; puissance <= NEUF; puissance++) {
            for (int couleur = VERT; couleur <= BRUN; couleur++) {
                CarteClan* carte = new CarteClan(static_cast<Puissance>(puissance), static_cast<Couleur>(couleur));
                pioche.push_back(carte);
                initialPioche[i] = carte;
                ++i;
            }
        }
    }

    PiocheClan::~PiocheClan() {
        delete[] initialPioche;
    }

    PiocheTactique::PiocheTactique() {
        const int taillePioche = 10; // taille de la pioche Tactique
        initialPioche = new const CarteTactique * [taillePioche];
        int i = 0;
        for (int typeTactique = JOKER1; typeTactique <= TRAITRE; typeTactique++) {
            CarteTactique* carte = new CarteTactique(static_cast<Capacite>(typeTactique));
            pioche.push_back(carte);
            initialPioche[i] = carte;
            ++i;
        }
    }

    PiocheTactique::~PiocheTactique() {
        delete[] initialPioche;
    }
}