#pragma once
#include "outils.h"
#include "EnsembleCartes.h"
#include "Carte.h"
#include "StrategieJoueur.h"
namespace SchottenTotten {
    class Partie; // forward declaration
    class Frontiere;
    class Joueur {
        int numJoueur{ 0 }; //joueur "1" ou joueur "2"
        std::vector<const Carte*> cartesEnMain;
        int nbCartesEnMain[2] = { 0, 0 };
            // Sauvegarde le nombre de carte Clan & Tactique.
        int nbCartesTactiqueJouees{ 0 };
        StrategieJoueur* strategieJoueur{new StrategieManuelle};
        int score{ 0 };
    public:
        void printCartes();
        void addCarte(const Carte* carte);
        void removeCarte(Carte* carte);
        void removeCarte(int index); // enlever plus tard
        int getNbClan() const { return nbCartesEnMain[CLAN]; }
        int getNbTactique() const { return nbCartesEnMain[TACTIQUE]; }
        // a changer plus tard, est-ce qu'on utilise plutot un enum?
        int getNbCartes() const { 
            int nbCartes = static_cast<int>(cartesEnMain.size());
            return nbCartes;
        }
        const Carte* getCarte(int index); // const ou pas?

        void getIndicesCartesClanEnMain(std::vector<int>& getVecteurCartesClan);

        // Design Pattern Strategy
        int choisirIndiceCarte() {return strategieJoueur->choisirIndiceCarte(getNbCartes());}
        int choisirIndiceBorne() { return strategieJoueur->choisirIndiceBorne();}
        int choisirPuissance() { return strategieJoueur->choisirPuissance(); }
        int choisirCouleur() { return strategieJoueur->choisirCouleur(); }
        int choisirSiTirerCarteTactique() { return strategieJoueur->choisirSiTirerCarteTactique(); }
        int choisir(int debut, int fin, const std::string& message) 
            { return strategieJoueur->choisir(debut, fin, message); }
        int choisirConstrained(const std::vector<int>& options) { return strategieJoueur->choisirConstrained(options); }
        void setStrategieJoueur(StrategieJoueur* strat) {
            strategieJoueur = strat;
        }

        int getNumJoueur() const { return numJoueur; }
        int getScore() const { return score; }
        void augScore() { score += 1; }
        int getNbCartesTactiqueJouees() const { return nbCartesTactiqueJouees; }
        void augNbCartesTactiqueJouees() { nbCartesTactiqueJouees += 1; }


        Joueur(int name) : numJoueur(name), score(0) {} //prend "1" ou "2" comme joueur 1 ou joueur 2 en argument.
        Joueur() : score(0) {}
    };
}

