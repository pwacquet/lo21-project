#include "Carte.h"
constexpr int LONGUEUR_AFFICHAGE = 7;

namespace SchottenTotten {
	TypeCarte CarteClan::quelTypeCarte() const {
		return CLAN;
	}
	TypeCarte CarteTactique::quelTypeCarte() const {
		return TACTIQUE;
	}
	void CarteClan::print() const {
		std::cout << *this;
	}
	void CarteTactique::print() const {
		std::cout << *this;
	}
	std::ostream& operator<< (std::ostream& strm, Puissance puiss) {
		const std::string namePuissance[] = { "ZERO", "UN", "DEUX", "TROIS", "QUATRE", "CINQ", "SIX", "SEPT", "HUIT", "NEUF" };
		return strm << namePuissance[puiss];
	}
	std::ostream& operator<< (std::ostream& strm, Couleur couleur)
	{
		const std::string nameCouleur[] = { "VERT", "BLEU", "ROUGE", "JAUNE", "VIOLET", "BRUN" };
		return strm << nameCouleur[couleur];
	}
	std::ostream& operator<< (std::ostream& strm, Capacite tac)
	{
		const std::string typeTactique[] = { "JOKER1", "JOKER2", "ESPION", "PORTEBOUCLIER", "COLINMAILLLARD", "COMBATDEBOUE", "CHASSEUR", "STRATEGE", "BANSHEE", "TRAITRE" };
		return strm << typeTactique[tac];
	}

	std::ostream& operator<< (std::ostream& strm, const CarteClan& carte) {
		if (&carte == nullptr) return strm << "[     ]";
		switch (carte.couleur) {
		case VERT:
			strm << "\033[48;5;28m\033[38;5;231m"; // couleur de fond et de police
			strm << std::left << std::setw(LONGUEUR_AFFICHAGE) << carte.puissance;
			strm << "\033[0m   "; // reinitialiser la couleur
			break;
		case BLEU:
			strm << "\033[48;5;33m\033[38;5;231m";
			strm << std::left << std::setw(LONGUEUR_AFFICHAGE) << carte.puissance << "\033[0m   ";
			break;
		case ROUGE:
			strm << "\033[48;5;196m\033[38;5;231m";
			strm << std::left << std::setw(LONGUEUR_AFFICHAGE) << carte.puissance << "\033[0m   ";
			break;
		case JAUNE:
			strm << "\033[48;5;226m\033[38;5;16m";
			strm << std::left << std::setw(LONGUEUR_AFFICHAGE) << carte.puissance << "\033[0m   ";
			break;
		case VIOLET:
			strm << "\033[48;5;129m\033[38;5;231m";
			strm << std::left << std::setw(LONGUEUR_AFFICHAGE) << carte.puissance << "\033[0m   ";
			break;
		case BRUN:
			strm << "\033[48;5;94m\033[38;5;231m";
			strm << std::left << std::setw(LONGUEUR_AFFICHAGE) << carte.puissance << "\033[0m   ";
			break;
		}
		return strm;
	}
	std::ostream& operator<< (std::ostream& strm, const CarteTactique& carte) {
		if (&carte == nullptr) strm << "[     ]";
		else {
			strm << "\033[48;5;202m\033[38;5;231m";
			strm << std::left << std::setw(LONGUEUR_AFFICHAGE) << carte.typeTactique << "\033[0m   ";
		}
		return strm;
	}
}

